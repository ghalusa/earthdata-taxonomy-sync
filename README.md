# Earthdata Taxonomy Sync

## Contents of this File

* Introduction
* Requirements
* Installation
* Usage
* Maintainers


### Introduction

The Earthdata Taxonomy Sync module imports and/or synchronizes taxonomy terms from [NASA's GCMD Keyword Management Service (KMS) API](https://wiki.earthdata.nasa.gov/display/gcmdkey/Keyword+Management+Service+Application+Program+Interface).

Keyword Viewer: https://gcmd.earthdata.nasa.gov/KeywordViewer/scheme/all?gtm_scheme=all

### Requirements

* N/A


### Installation

* Install as you would normally install a contributed Drupal module.
   See: [Installing Modules](https://www.drupal.org/docs/extending-drupal/installing-modules) for further information.

### Usage

NASA's GCMD taxonomy terms can be imported and synchronized using the custom Drush command, `drush taxsync`. For more information, please see [these instructions](https://gist.github.com/ghalusa/d08e0cbc49d12c76d8def8e1cc4a7f36).

**Note:** The ability to execute the synchronization process within Drupal's admin is in the works.


### Maintainers/Support

* Goran Halusa: [goran.n.halusa@nasa.gov](mailto:goran.n.halusa@nasa.gov)
