<?php

namespace Drupal\earthdata_taxonomy_sync\Commands;

use Drush\Commands\DrushCommands;
use Drupal\taxonomy\Entity\Term;
use Symfony\Component\Serializer\NameConverter\CamelCaseToSnakeCaseNameConverter;
use Drupal\Component\Utility\UrlHelper;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\pathauto\PathautoState;
use Drupal\Component\Utility\Html;

/**
 * A drush command file.
 *
 * @package Drupal\earthdata_taxonomy_sync\Commands
 * See: https://www.droptica.com/blog/creating-custom-drush-9-commands-drupal-8/
 */
class Drush9CustomCommands extends DrushCommands {

  /**
   * The entity type manager service.
   *
   * @var EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The parent terms.
   *
   * @var parentTargets
   */
  protected $parentTargets;

  /**
   * The parent terms.
   *
   * @var uriEarthScience
   */
  protected $uriEarthScience;

  /**
   * The parent terms.
   *
   * @var uriEarthScienceServices
   */
  protected $uriEarthScienceServices;

  /**
   * Constructor.
   */
  public function __construct() {

    // Drupal's entityTypeManager.
    $this->entityTypeManager = \Drupal::entityTypeManager();
    // This module's configuration set by the end-user via the admin page: admin/config/earthdata/content/import
    $config = \Drupal::config('earthdata_taxonomy_sync.settings');
    // Get parent targets set by the end-user via the admin page: admin/config/earthdata/content/import
    $this->parentTargets = [];
    $earth_science = $config->get('gcmd_options_earth_science');
    $earth_science_services = $config->get('gcmd_options_earth_science_services');
    if (!empty($earth_science) && !empty($earth_science_services)) {
      $this->parentTargets = array_merge($earth_science, $earth_science_services);
    }
    // GCMD URIs
    $this->gcmdGetConceptsByIdUri = 'https://gcmd.earthdata.nasa.gov/kms/concept/';
    $this->format = '?format=json';
    $this->uriEarthScience = $this->gcmdGetConceptsByIdUri . 'e9f67a66-e9fc-435c-b720-ae32a2c3d8f5' . $this->format;
    $this->uriEarthScienceServices = $this->gcmdGetConceptsByIdUri . '894f9116-ae3c-40b6-981d-5113de961710' . $this->format;

  }

   // * @param string $text
   // *   Argument with message to be displayed.

  /**
   * Drush command which runs Earthdata's taxonomy sync.
   *
   * @command earthdata_taxonomy_sync:message
   * @aliases taxsync
   * @option ess
   *   Import from the parent term, Earth Science Services.
   * @usage earthdata_taxonomy_sync:sync --ess
   */
  public function sync($options = ['ess' => FALSE]) {

    if (empty($this->parentTargets)) {
      // Set the message.
      $msg = 'Go to admin/config/earthdata/content/import and choose at least one GCMD Science Keyword.';
      // Log to the database.
      \Drupal::logger('earthdata_taxonomy_sync')->error($msg);
      return;
    }

    $operations = [];
    $vocabulary = 'gcmd';
    // $vocabulary = $this->config->get('targeted_vocabulary');

    // Use the EARTH SCIENCE SERVICES uri if the '--ess' flag is passed. Otherwise, use the EARTH SCIENCE uri.
    $uri = $options['ess'] ? $this->uriEarthScienceServices : $this->uriEarthScience;
    // For the database log message.
    $top_level_concept = $options['ess'] ? 'Earth Science Services' : 'Earth Science';

    // Log to the database.
    \Drupal::logger('earthdata_taxonomy_sync')->info('Executed taxonomy syncronization process. Top Level Concept: ' . $top_level_concept);

    // See if the input is a URL. Fetch and validate JSON data from a feed.
    if (UrlHelper::isValid($uri) && UrlHelper::isExternal($uri)) {
      try {

        $fetchDataFeed = \Drupal::service('earthdata_taxonomy_sync.fetch_data_feed');
        $jsonData = $fetchDataFeed->fetchJsonData($uri);

        if ($jsonData === NULL) {
          $form_state->setErrorByName('data_input', $this->t('Unable to fetch JSON data from a feed.'));
          $this->output()->writeln('Unable to fetch JSON data from the feed.');
        }
      }
      catch(\Exception $e) {
        $jsonData = NULL;
        $this->output()->writeln($e->getMessage());
      }
    }
    else {
      if ($jsonData === NULL) {
        $this->output()->writeln('Invalid JSON data.');
      }
    }

    // We may want the count at some point, perhaps for reporting.
    $total = count($jsonData['narrower']);
    // Terms reside within 'narrower'.
    $terms = $jsonData['narrower'];

    // $this->output()->writeln($total);
    // return;

    // Process chosen parent terms in $this->parentTargets (Set by the end-user here: admin/config/earthdata/content/import).
    foreach ($terms as $term_data) {
      // Import child terms which belong to parent terms within the parentTargets array.
      if (array_key_exists($term_data['uuid'], $this->parentTargets) && ($this->parentTargets[$term_data['uuid']] !== 0)) {
        $this->processTerms($vocabulary, $term_data, NULL);
      }
    }

  }

  /**
   * @param string $vocabulary
   *   The bundle (e.g. tags, gcmd, foo, bar & etc).
   * @param array $term_data
   *   An associative array containing taxonomy term data.
   * @param string $parent_uuid
   *   The parent term's uuid.
   */
  private function processTerms($vocabulary, $term_data = [], $parent_uuid = NULL) {

    /** @var \Drupal\taxonomy\Entity\Term $term */
    $term = [];
    $child_term_data = [];

    // If passed, set the parent_uuid variable.
    if (!empty($parent_uuid)) {
      $term_data['parent_uuid'] = $parent_uuid;
    }

    if (is_array($term_data)) {

      // Transform the uppercase prefLabel to title case.
      // @TODO: Fix terms which contain parenthesis, foo, bar, etc...
      $term_title_case = ucwords( strtolower($term_data['prefLabel']), " \t\r\n\f\v'" );
      // Load the existing term.
      $term = reset( $term );
      $term = $this->entityTypeManager
        ->getStorage('taxonomy_term')
        ->loadByProperties(['field_gcmd_uuid' => $term_data['uuid']]);

      // Process the term.
      $this->processTerm($vocabulary, $term, $term_title_case, $term_data);
      // Only process child terms if the term is not a leaf (the last term in a tree).
      if ($term_data['isLeaf'] === FALSE) {
        // Get the child terms of a parent term.
        $child_term_data = $this->getChildTerms($term_data);
        // Traverse through child terms.
        if (!empty($child_term_data)) {
          foreach ($child_term_data as $key => $value) {
            // Process terms.
            $this->processTerms($vocabulary, $value, $value['parent_uuid'], TRUE);
          }
        }

      }

    }

  }

  /**
   * @param string $vocabulary
   *   The bundle (e.g. tags, gcmd, foo, bar & etc).
   * @param array $term
   *   An associative array containing taxonomy term data.
   * @param string $term_title_case
   *   The term transformed to title case.
   * @param array $term_data
   *   An associative array containing taxonomy term data.
   */
  private function processTerm($vocabulary, $term, $term_title_case, $term_data) {

    // Throttle API calls by 100 miliseconds (100000 microseconds).
    // See: 2.1 Connections
    // https://wiki.earthdata.nasa.gov/display/gcmdkey/Keyword+Management+Service+Application+Program+Interface#KeywordManagementServiceApplicationProgramInterface-2.1Connections
    usleep(500000);

    $data = [];
    $message = '';

    // Get this term's definition.
    $fetchDataFeed = \Drupal::service('earthdata_taxonomy_sync.fetch_data_feed');
    $uri = $this->gcmdGetConceptsByIdUri . $term_data['uuid'] . $this->format;
    $result = $fetchDataFeed->fetchJsonData($uri);
    $term_data['definition'] = !empty($result['definitions']) ? $result['definitions'][0]['text'] : 'Definition unavailable';

    // Insert a new taxonomy term.
    if (empty($term)) {

      // Initialize a new term.
      $new_term = Term::create(
        [
          'name' => $term_title_case,
          'vid' => strtolower($vocabulary),
          'description' => [
            'value' => $term_data['definition'],
            'format' => 'full_html',
          ],
          'field_gcmd_id' => (string)$term_data['id'],
          'field_gcmd_uuid' => $term_data['uuid'],
          'field_gcmd_pref_label' => $term_data['prefLabel'],
          'field_gcmd_concept_scheme' => $term_data['scheme']['shortName'],
          'field_gcmd_is_leaf' => $term_data['isLeaf'] ? 'true' : 'false',
        ]
      );

      // Set the parent term and URL alias.
      if (array_key_exists('parent_uuid', $term_data)) {

        // Get the parent term's tid.
        $query = $this->entityTypeManager
          ->getStorage('taxonomy_term')
          ->getQuery();

        $parent = $query->condition('field_gcmd_uuid', $term_data['parent_uuid'])
          ->execute();

        // Catch for an empty parent, which should not occur.
        if (empty($parent)) {
          // Log to the database.
          $msg = 'parent_term is null: ' . $term_data['prefLabel'] . ' - ' . $term_data['parent_uuid'];
          \Drupal::logger('earthdata_taxonomy_sync')->error($msg);
        }

        if (!empty($parent)) {

          // Extract the parent_id
          $parent_id = array_values($parent);

          // Set the child term's parent id.
          // Example query result, key => value: [572 => "572"]. Use the value, $parent_id[0].
          $new_term->set('parent', [ 'target_id' => $parent_id[0] ]);

          // Load the parent term.
          $parent_term = Term::load( $parent_id[0] );
        }

      }

      // Patch for re-enabling Pathauto once again for Topic (earthdata) terms.
      $new_term->path->pathauto = PathautoState::CREATE;
      // Save the new taxonomy term.
      $new_term->save();

      // Set the message.
      $data['message'] = t(
        'Processed New Term: @name, TID: @tid, GCMD UUID: @gcmd_uuid.',
        ['@name' => $term_title_case, '@tid' => $new_term->id(), '@gcmd_uuid' => $new_term->get('field_gcmd_uuid')->value]
      );

    }

    // Update a term if anything changed.
    if (!empty($term)) {

      $data = $diff = [];

      // Load the existing term.
      $term = reset( $term );
      $existing_term = Term::load( $term->id() );

      // Patch for re-enabling Pathauto once again for Topic (earthdata) terms.
      $existing_term->path->pathauto = PathautoState::CREATE;
      $existing_term->save();

      // Create the term slug for the URL alias.
      $term_slug = Html::getClass($term_title_case);

      // Patch for removing top level term URL aliases.
      if (!array_key_exists('parent_uuid', $term_data)) {

        // Get the existing alias.
        $path_alias_manager = \Drupal::service('path_alias.manager');
        $alias = $path_alias_manager->getAliasByPath('/topic/' . $term_slug);
        
        if (!empty($alias)) {
          $path_alias_storage = \Drupal::service('entity_type.manager')->getStorage('path_alias');
          $entities = $path_alias_storage->loadByProperties(['path' => $alias]);

          if (!empty($entities)) {
            // Delete the url alias.
            $path_alias_storage->delete($entities);
            // Set the message.
            $url_alias_message = t(
              'Top level term URL alias removed for @name, TID: @tid, GCMD UUID: @gcmd_uuid.',
              ['@name' => $term_title_case, '@tid' => $existing_term->id(), '@gcmd_uuid' => $existing_term->get('field_gcmd_uuid')->value]
            );
            // Log to the database.
            \Drupal::logger('earthdata_taxonomy_sync')->info($url_alias_message);
            // Output the message to the console.
            $this->output()->writeln($url_alias_message);
          }
        }
      }

      // Patch for removing child level term URL aliases.
      if (array_key_exists('parent_uuid', $term_data)) {

        // Get the parent term's tid.
        $query = $this->entityTypeManager
          ->getStorage('taxonomy_term')
          ->getQuery();

        $parent = $query->condition('field_gcmd_uuid', $term_data['parent_uuid'])
          ->execute();

        if (!empty($parent)) {
          // Extract the parent_id
          $parent_id = array_values($parent);
          // Load the parent term.
          $parent_term = Term::load( $parent_id[0] );
          // Create the term slug for the URL alias.
          $parent_term_slug = Html::getClass($parent_term->get('name')->value) . '/';
          // Get the existing alias.
          $path_alias_manager = \Drupal::service('path_alias.manager');
          $alias = $path_alias_manager->getAliasByPath('/topic/' . $parent_term_slug . strtolower($term_slug));
          

          // dump('got here 1');


          if (!empty($alias)) {


            // dump('got here 2');


            // Get the url aliases.
            $path_alias_storage = \Drupal::service('entity_type.manager')->getStorage('path_alias');
            $entities = $path_alias_storage->loadByProperties(['path' => $alias]);

            if (!empty($entities)) {
              // Delete the url alias.
              $delete = $path_alias_storage->delete($entities);

              $this->output()->writeln('[DELETED CHILD URL ALIAS] ' . $term_title_case);

              // If deleted, log messages to the database and console.
              // Set the message.
              $url_alias_message = t(
                'Child term URL alias removed for @name, TID: @tid, GCMD UUID: @gcmd_uuid.',
                ['@name' => $term_title_case, '@tid' => $existing_term->id(), '@gcmd_uuid' => $existing_term->get('field_gcmd_uuid')->value]
              );
              // Log to the database.
              \Drupal::logger('earthdata_taxonomy_sync')->info($url_alias_message);
              // Output the message to the console.
              $this->output()->writeln($url_alias_message);
            }
          }
        }

      }

      // Default message.
      $data['message'] = t(
        'Checking Existing Term: @name, TID: @tid.',
        ['@tid' => $existing_term->id(), '@name' => $term_title_case]
      );

      // If the uuids are identical, process...
      if ($term_data['uuid'] === $existing_term->get('field_gcmd_uuid')->value) {

        // New GCMD field values.
        $new_gcmd_fields = [
          'id' => $term_data['id'],
          'uuid' => $term_data['uuid'],
          'prefLabel' => $term_data['prefLabel'],
          'isLeaf' => $term_data['isLeaf'],
          'description' => $term_data['definition'],
        ];

        // Existing GCMD field values.
        $existing_gcmd_fields = [
          'id' => $existing_term->get('field_gcmd_id')->value,
          'uuid' => $existing_term->get('field_gcmd_uuid')->value,
          'prefLabel' => $existing_term->get('field_gcmd_pref_label')->value,
          'isLeaf' => ($existing_term->get('field_gcmd_is_leaf')->value === 'true') ? true : false,
          'description' => $existing_term->get('description')->value,
        ];

        // Compute differences of field values.
        $diff = array_diff($new_gcmd_fields, $existing_gcmd_fields);

        // If there are no differences...
        if (empty($diff)) {
          // Set the message.
          $data['message'] = t(
            'Skipped Exisiting Term: No changes detected for "@name", TID: @tid, GCMD UUID: @gcmd_uuid.',
            ['@name' => $term_title_case, '@tid' => $existing_term->id(), '@gcmd_uuid' => $existing_term->get('field_gcmd_uuid')->value]
          );
        }

        // If differences exist...
        if (!empty($diff)) {
          // Loop through field values which have changed and set the new value.
          foreach ($diff as $key => $value) {
            // Convert camel case (GCMD field) to snake case (Drupal field).
            $converter = new CamelCaseToSnakeCaseNameConverter();
            $normalized_key = ($key !== 'description') ? 'field_gcmd_' . $converter->normalize($key) : $key;
            // Set the new value.
            $existing_term->$normalized_key->setValue($value);
            // Update the taxonomy term.
            if ($key === 'prefLabel') {
              $updated_term = ucwords( strtolower($value), " \t\r\n\f\v'" );
              $existing_term->name->setValue($updated_term);
            }
          }
          // Record when the taxonomy term was last updated.
          $existing_term->description->setValue($term_data['definition']);

          // Patch for re-enabling Pathauto once again for Topic (earthdata) terms.
          $existing_term->path->pathauto = PathautoState::CREATE;
          // Save the taxonomy term.
          $existing_term->save();

          // Set the message.
          $data['message'] = t(
            'Processed Existing Term: Changes detected for @name, TID: @tid, GCMD UUID: @gcmd_uuid.',
            ['@name' => $term_title_case, '@tid' => $existing_term->id(), '@gcmd_uuid' => $existing_term->get('field_gcmd_uuid')->value]
          );
          // Log to the database.
          \Drupal::logger('earthdata_taxonomy_sync')->info($data['message']);
        }

      }

    }

    // Log to the database.
    // \Drupal::logger('earthdata_taxonomy_sync')->info($data['message']);
    // Output the message to the console.
    if (!empty($data['message'])) {
      $this->output()->writeln($data['message']);
    }

  }

  /**
   * Fetches child terms in JSON format from a given feed.
   *
   * @param string $data JSON data feed.
   *
   * @return array An array containing child terms.
   */
  private function getChildTerms($jsonData = NULL) {

    $data = $children = $result = [];

    if (is_array($jsonData) && !empty($jsonData)) {

      $fetchDataFeed = \Drupal::service('earthdata_taxonomy_sync.fetch_data_feed');

      // Get terms.
      $uri = $this->gcmdGetConceptsByIdUri . $jsonData['uuid'] . $this->format;

      try {
        $result = $fetchDataFeed->fetchJsonData($uri);
        if ($result === NULL) {
          $data['error'] = t('Unable to fetch JSON data from a feed.');
        }
      }
      catch(\Exception $e) {
        $data['error'] = $e->getMessage();
      }

    }

    if (array_key_exists('narrower', $result)) {

      $children = $data = $result['narrower'];

      if (!empty($children)) {
        foreach ($children as $key => $value) {
          $data[$key]['parent_uuid'] = $jsonData['uuid'];
        }
      }
      
    }

    return $data;
  }
  
}