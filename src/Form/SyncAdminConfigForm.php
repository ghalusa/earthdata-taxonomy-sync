<?php

namespace Drupal\earthdata_taxonomy_sync\Form;

use Drupal\Component\Plugin\Exception\PluginException;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StreamWrapper\PublicStream;
use Symfony\Component\DependencyInjection\ContainerInterface;


/**
 * Class SyncAdminConfigForm
 * Provides admin interface for syncing taxonomy terms.
 *
 * @category AdminUIForm
 * @package Drupal\earthdata_taxonomy_sync\Form
 * @author Goran Halusa <goran.halusa@ssaihq.com>
 */
class SyncAdminConfigForm extends ConfigFormBase {

  const CONFIG_NAME = 'earthdata_taxonomy_sync.settings';
  const DEFAULT_TARGETED_VOCABULARY = 'gcmd';
  const GCMD_EARTH_SCIENCE = 'https://gcmd.earthdata.nasa.gov/kms/concept/e9f67a66-e9fc-435c-b720-ae32a2c3d8f5?format=XML';
  const GCMD_EARTH_SCIENCE_SERVICES = 'https://gcmd.earthdata.nasa.gov/kms/concept/894f9116-ae3c-40b6-981d-5113de961710?format=XML';

  /**
   * The entity type manager service.
   *
   * @var EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * @inheritDoc
   */
  public function getFormId() {
    return 'earthdata_taxonomy_sync_admin_config';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      self::CONFIG_NAME,
    ];
  }

  /**
   * Constructs a \Drupal\earthdata_taxonomy_sync\Form\SyncAdminConfigForm object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   * @param EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager service.
   */
  public function __construct(ConfigFactoryInterface $config_factory, EntityTypeManagerInterface $entity_type_manager) {
    parent::__construct($config_factory);
    $this->entityTypeManager = $entity_type_manager;
    $this->fetchDataFeed = \Drupal::service('earthdata_taxonomy_sync.fetch_data_feed');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * @inheritDoc
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $config = $this->config(self::CONFIG_NAME);
    $form = parent::buildForm($form, $form_state);

    $form['vocabulary_container'] = [
      '#type' => 'details',
      '#title' => $this->t('Vocabulary'),
      '#open' => TRUE,
    ];

    $vocabulary = $config->get('targeted_vocabulary');
    $form['vocabulary_container']['targeted_vocabulary'] = [
      '#type' => 'radios',
      '#title' => $this->t('Default Target Vocabulary'),
      '#description' => $this->t('Choose a target vocabulary.'),
      '#default_value' => !empty($vocabulary) ? $vocabulary : self::DEFAULT_TARGETED_VOCABULARY,
      '#options' => self::getVocabularyOptions(),
      '#ajax' => [
        'callback' => '::gcmdConceptOptionsCallback',
        'wrapper' => 'checkboxes-container',
      ],
      '#required' => TRUE,
    ];

    $form['target'] = [
      '#type' => 'details',
      '#title' => $this->t('GCMD Terms'),
      '#open' => ($vocabulary !== NULL) ? TRUE : FALSE,
      '#states' => [
        'visible' => [
          ':input[name="targeted_vocabulary"]' => ['value' => 'gcmd'],
        ],
      ],
    ];

    $form['target']['checkboxes_container'] = [
      '#type' => 'container',
      '#attributes' => ['id' => 'checkboxes-container'],
      '#states' => [
        'visible' => [
          ':input[name="targeted_vocabulary"]' => ['value' => 'gcmd'],
        ],
      ],
    ];

    // GCMD SCIENCE KEYWORDS > EARTH SCIENCE Concepts.
    $selectedEarthScienceOptions = $config->get('gcmd_options_earth_science');
    $form['target']['gcmd_options_earth_science'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('SCIENCE KEYWORDS > EARTH SCIENCE'),
      '#description' => $this->t('This listing is derived from <a href=":link1" target="_blank">SCIENCE KEYWORDS > EARTH SCIENCE</a>.', [':link1' => static::GCMD_EARTH_SCIENCE]),
      '#default_value' => ($selectedEarthScienceOptions !== NULL) ? $selectedEarthScienceOptions : [],
      '#options' => $this->getEarthScienceCheckboxes(),
      '#required' => TRUE,
      '#states' => [
        'visible' => [
          ':input[name="targeted_vocabulary"]' => ['value' => 'gcmd'],
        ],
      ],
    ];

    // GCMD SCIENCE KEYWORDS > EARTH SCIENCE SERVICES Concepts.
    $selectedEarthScienceServicesOptions = $config->get('gcmd_options_earth_science_services');
    $form['target']['gcmd_options_earth_science_services'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('SCIENCE KEYWORDS > EARTH SCIENCE SERVICES'),
      '#description' => $this->t('This listing is derived from <a href=":link2" target="_blank">SCIENCE KEYWORDS > EARTH SCIENCE SERVICES</a>.', [':link2' => static::GCMD_EARTH_SCIENCE_SERVICES]),
      '#default_value' => ($selectedEarthScienceServicesOptions !== NULL) ? $selectedEarthScienceServicesOptions : [],
      '#options' => $this->getEarthScienceServicesCheckboxes(),
      '#required' => TRUE,
      '#states' => [
        'visible' => [
          ':input[name="targeted_vocabulary"]' => ['value' => 'gcmd'],
        ],
      ],
    ];

    // Default concepts HTML markup.
    $form['default_concepts'] = [
      '#type' => 'details',
      '#title' => $this->t('Default GCMD Concepts'),
      '#open' => TRUE,
    ];

    $earth_science_list = $earth_science_services_list = '';
    $config = \Drupal::config('earthdata_taxonomy_sync.settings');
    $earth_science = $config->get('default_earth_science_checkboxes');
    $earth_science_services = $config->get('default_earth_science_services_checkboxes');

    foreach ($earth_science as $es_value) {
      $earth_science_list .= '<li>' . $es_value . '</li>' . "\n";
    }

    foreach ($earth_science_services as $ess_value) {
      $earth_science_services_list .= '<li>' . $ess_value . '</li>' . "\n";
    }

    $form['default_concepts']['markup'] = [
      '#type' => 'markup',
      '#markup' => '<p>These are the default GCMD concepts (in the event there are no selected checkboxes above):</p>
        <h3>Earth Science</h3>
        <ul>' . $earth_science_list . '</ul>
        <h3>Earth Science Services</h3>
        <ul>' . $earth_science_services_list . '</ul>',
    ];

    return $form;
  }

  /**
   * @inheritDoc
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config(self::CONFIG_NAME);    
    // $config->set('limit_taxonomy_terms_per_batch', $form_state->getValue('limit_taxonomy_terms_per_batch'));
    $config->set('targeted_vocabulary', $form_state->getValue('targeted_vocabulary'));
    $config->set('gcmd_options_earth_science', $form_state->getValue('gcmd_options_earth_science'));
    $config->set('gcmd_options_earth_science_services', $form_state->getValue('gcmd_options_earth_science_services'));
    // $config->set('example_url', trim($form_state->getValue('example_url')));
    // $config->set('example_data', trim($form_state->getValue('example_data')));
    $config->save();
    return parent::submitForm($form, $form_state);
  }

  /**
   * Updates additional field selection table.
   *
   * This callback will occur *after* the form has been rebuilt by buildForm().
   * Since that's the case, the form should contain the right values for
   * additional field selection table.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return array
   *   The portion of the render structure that will replace the
   *   target-container form element.
   */
  public function gcmdConceptOptionsCallback(array $form, FormStateInterface $form_state) {
    return $form['target']['checkboxes_container'];
  }

  /**
   * @return array An associative array suitable for #options.
   */
  private function getVocabularyOptions() {
    $options = [];

    try {
      $types = $this->entityTypeManager
        ->getStorage('taxonomy_vocabulary')
        ->loadMultiple();

      foreach ($types as $t) {
        if ($t->get('originalId') === 'gcmd') {
          $options[$t->id()] = $t->label() . " ({$t->id()})";
        }
      }
    }
    catch (PluginException $e) {
      // no code
    }

    return $options;
  }

  /**
   * @return array An associative array for select options.
   */
  private function getEarthScienceCheckboxes() {
    $options = [];

    // SCIENCE KEYWORDS > EARTH SCIENCE
    try {

      $esXmlData = $this->fetchDataFeed->fetchXmlData(static::GCMD_EARTH_SCIENCE);
      if ($esXmlData === NULL) {
        $form_state->setErrorByName('gcmd_options_earth_science', $this->t('Unable to fetch EARTH SCIENCE XML data.'));
      } else {

        foreach ($esXmlData['narrower']['conceptBrief'] as $value) {
          $options[$value['@attributes']['uuid']] = $value['@attributes']['prefLabel'];
        }

        asort($options);
      }
    }
    catch(\Exception $e) {
      $esXmlData = NULL;
      $form_state->setErrorByName('gcmd_options_earth_science', $e->getMessage());
    }

    return $options;
  }

  /**
   * @return array An associative array for select options.
   */
  private function getEarthScienceServicesCheckboxes() {
    $options = [];

    // SCIENCE KEYWORDS > EARTH SCIENCE SERVICES
    try {

      $essXmlData = $this->fetchDataFeed->fetchXmlData(static::GCMD_EARTH_SCIENCE_SERVICES);
      if ($essXmlData === NULL) {
        $form_state->setErrorByName('gcmd_options_earth_science_services', $this->t('Unable to fetch EARTH SCIENCE SERVICES XML data.'));
      } else {

        foreach ($essXmlData['narrower']['conceptBrief'] as $value) {
          $options[$value['@attributes']['uuid']] = $value['@attributes']['prefLabel'];
        }

        asort($options);
      }
    }
    catch(\Exception $e) {
      $essXmlData = NULL;
      $form_state->setErrorByName('gcmd_options_earth_science_services', $e->getMessage());
    }

    return $options;
  }

}

