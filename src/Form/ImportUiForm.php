<?php

namespace Drupal\earthdata_taxonomy_sync\Form;

use Drupal\Component\Plugin\Exception\PluginException;
use Drupal\Component\Utility\Html;
use Drupal\Component\Utility\UrlHelper;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class ImportUiForm
 * Provides admin interface for syncing taxonomy terms.
 *
 * @category AdminUIForm
 * @package Drupal\earthdata_taxonomy_sync\Form
 * @author Goran Halusa <goran.halusa@ssaihq.com>
 */
class ImportUiForm extends FormBase {

  //@TODO make configurable.
  const GCMD_TOP_LEVEL_CONCEPTS = 'https://gcmd.earthdata.nasa.gov/kms/concepts/root?format=XML';
  const GCMD_KMS_API_URL = 'https://gcmd.earthdata.nasa.gov/kms/concept/';
  const DEPTH = 4;

  /**
   * The entity type manager service.
   *
   * @var EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Configurations saved using the admin form.
   *
   * @var \Drupal\Core\Config\Config
   */
  protected $config;

  /**
   * Constructor.
   *
   * @param EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager service.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager) {
    $this->entityTypeManager = $entity_type_manager;
    $this->config = $this->config(SyncAdminConfigForm::CONFIG_NAME);
    $this->fetchDataFeed = \Drupal::service('earthdata_taxonomy_sync.fetch_data_feed');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('entity_field.manager')
    );
  }

  /**
   * @inheritDoc
   */
  public function getFormId() {
    return 'earthdata_taxonomy_sync_admin_ui';
  }

  /**
   * @inheritDoc
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    // Concepts
    $concept_options = ['Earth Science', 'Earth Science Services'];
    
    $form['concept'] = [
      '#type' => 'select',
      '#title' => $this->t('GCMD Top-Level Concept'),
      '#options' => $concept_options,
      '#default_value' => 0,
      '#required' => TRUE,
      '#prefix' => '<div data-drupal-messages="">
        <div role="contentinfo" aria-label="Status message" class="messages">
          <h2>Under Construction</h2>
            <p>The ability to execute the GCMD taxonomy import from Drupal\'s admin is coming soon!</p>
            <p>For now, please use the drush commands, <code>drush taxsync</code> for "Earth Science" terms and <code>drush taxsync --ess</code> for "Earth Science Services" terms.</p>
          </div>
        </div>',
    ];

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Process'),
      '#disabled' => TRUE,
    ];

    return $form;
  }

  /**
   * @inheritDoc
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    // $this->flushTerms();

    $concept = $form_state->getValue('concept');
    $flag = ($concept === '1') ? ' --ess' : '';

    $output = shell_exec('drush taxsync' . $flag);
    echo '<pre>' . $output . '</pre>';

  }

  /**
   * Flushes all taxonomy terms for a given vocabulary.
   * WARNING: This is a destructive process, only to be used only during development.
   */
  public function flushTerms($vocabulary = 'gcmd') {

    $tids = \Drupal::entityQuery('taxonomy_term')
      ->condition('vid', $vocabulary)
      ->execute();
    $controller = \Drupal::entityTypeManager()->getStorage('taxonomy_term');
    $entities = $controller->loadMultiple($tids);
    $controller->delete($entities);

    dd('Taxonomy terms flushed for the ' . $vocabulary . ' vocabulary.');

  }

}
